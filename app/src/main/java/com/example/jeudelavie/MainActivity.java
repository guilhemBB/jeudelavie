package com.example.jeudelavie;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    int taille = 170;


    ArrayList<ArrayList<ImageView>> grilleIV;
    ArrayList<ArrayList<Integer>> grillenbVois;
    ArrayList<ArrayList<Integer>> grilleBooleanNew;
    ConstraintLayout ecran;
    TextView cpt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ecran = findViewById(R.id.screen);
        grilleIV = new ArrayList<>();
        grilleBooleanNew = new ArrayList<>();
        grillenbVois = new ArrayList<>();
        LinearLayout matL = new LinearLayout(this);

        for(int i=0;i<taille;i++){
            LinearLayout layout = new LinearLayout(MainActivity.this);
            ArrayList<ImageView> ligne = new ArrayList<>();
            ArrayList<Integer> ligneBooleanNew = new ArrayList<>();
            ArrayList<Integer> lignenbVois = new ArrayList<>();
            LinearLayout lignL = new LinearLayout(this);
            lignL.setOrientation(LinearLayout.VERTICAL);
            grilleIV.add(ligne);
            grilleBooleanNew.add(ligneBooleanNew);
            grillenbVois.add(lignenbVois);
            for(int j=0; j<taille; j++){
                ImageView px = new ImageView(this);
                Boolean bl;
                ligne.add(px);

                ligneBooleanNew.add(1);
                lignenbVois.add(1);
                px.setImageResource(R.drawable.blanc);
                int finalI = i;
                int finalJ = j;
                px.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int newVal = (grilleBooleanNew.get(finalI).get(finalJ)%4 + 1) ;
                        grilleBooleanNew.get(finalI).set(finalJ,newVal);
                        updateImage();
                        Log.e("wall","i am "+ finalI + " " + finalJ+"i am "+ grilleBooleanNew.get(finalI).get(finalJ).toString());

                    }
                });
                LinearLayout pxL = new LinearLayout(this);
                pxL.addView(px);
                lignL.addView(pxL);

            }
            matL.addView(lignL);
        }

        ecran.addView(matL);
        updateImage();

        cpt = findViewById(R.id.textViewCOMPT);
    }


    public void updateImage(){
        for(int coll=0;coll<taille; coll++){
            for(int lign=0; lign<taille; lign++){
                int color = grilleBooleanNew.get(coll).get(lign);
                if(color==1){
                    grilleIV.get(coll).get(lign).setImageResource(R.drawable.c1);
                }
                if(color==2){
                    grilleIV.get(coll).get(lign).setImageResource(R.drawable.c2);
                }
                if(color==3){
                    grilleIV.get(coll).get(lign).setImageResource(R.drawable.c3);
                }
                if(color==4){
                    grilleIV.get(coll).get(lign).setImageResource(R.drawable.c4);
                }


            }
        }
    }



    public void calcule() {
        int up, down, right, left, ml, mc;
        for(int i=0; i<taille;i++){
            for(int j=0; j<taille; j++){
                ml=i; mc=j;
                up=(i+taille-1)%(taille);
                down=(i+taille+1)%(taille);
                right=(j+taille+1)%(taille);
                left=(j+taille-1)%(taille);

                int voisinEstSuivant=0;
                int myState = grilleBooleanNew.get(ml).get(mc);
                if(myState%4+1==grilleBooleanNew.get(up).get(left)) voisinEstSuivant+=1;
                if(myState%4+1==grilleBooleanNew.get(up).get(mc)) voisinEstSuivant+=1;
                if(myState%4+1==grilleBooleanNew.get(up).get(right)) voisinEstSuivant+=1;
                if(myState%4+1==grilleBooleanNew.get(ml).get(left)) voisinEstSuivant+=1;
                //if(grilleBooleanNew.get(ml).get(mc)) voisinEstSuivant+=1;
                if(myState%4+1==grilleBooleanNew.get(ml).get(right)) voisinEstSuivant+=1;
                if(myState%4+1==grilleBooleanNew.get(down).get(left)) voisinEstSuivant+=1;
                if(myState%4+1==grilleBooleanNew.get(down).get(mc)) voisinEstSuivant+=1;
                if(myState%4+1==grilleBooleanNew.get(down).get(right)) voisinEstSuivant+=1;


                if(i==7&&j==7){
                    Log.e("string", "j ai "+voisinEstSuivant+"voisin");
                }
                grillenbVois.get(i).set(j,voisinEstSuivant);
            }
        }
    }

    public void finirTick(){
        for(int i=0; i<taille;i++){
            for(int j=0; j<taille; j++){
                int myVois=grillenbVois.get(i).get(j);

                if(myVois>=3)
                    grilleBooleanNew.get(i).set(j, ( grilleBooleanNew.get(i).get(j) %4)+1 );

                grillenbVois.get(i).set(j,0);
            }
        }
    }



    public void onRandom(View view) {
        Random random = new Random();
        for(int coll=0; coll<taille; coll++){
            for(int lign=0; lign<taille; lign++){
                int rd = random.nextInt(5);
                grilleBooleanNew.get(coll).set(lign,rd);
                grillenbVois.get(coll).set(lign,0);
            }
        }
        updateImage();
    }

    public void onWhite(View view) {
        for(int coll=0; coll<taille; coll++){
            for(int lign=0; lign<taille; lign++){
                grilleBooleanNew.get(coll).set(lign,1);
                grillenbVois.get(coll).set(lign,0);
            }
        }
        updateImage();
    }

    public void onTick(View view) {
        calcule();
        finirTick();
        updateImage();
    }

    public void onGo(View view) {
        for(int time=0; time<200; time++){
            calcule();
            finirTick();
            //updateImage();
            for(int ii=0; ii<taille; ii++){
                for(int jj=0; jj<taille; jj++){
                    grilleIV.get(ii).get(jj).invalidate();
                }
            }
            //SystemClock.sleep(3);
            cpt.setText(time+"");

        }
        updateImage();
    }
}