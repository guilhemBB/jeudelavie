package com.example.jeudelavie;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;

public class HiddenActi extends AppCompatActivity {


    int taille = 12;
    int tailleB = 24;
    ArrayList<ArrayList<ImageView>> grilleIV;
    ArrayList<ArrayList<Integer>> grilleIntNew;
    ConstraintLayout ecran;
    int nbClick=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hidden);

        ecran = findViewById(R.id.ecran2);
        grilleIV = new ArrayList<>();
        grilleIntNew = new ArrayList<>();
        LinearLayout matL = new LinearLayout(this);

        for(int i=0;i<taille;i++){
            ArrayList<ImageView> ligne = new ArrayList<>();
            ArrayList<Integer> ligneIntNew = new ArrayList<>();
            LinearLayout lignL = new LinearLayout(this);
            lignL.setOrientation(LinearLayout.VERTICAL);
            grilleIV.add(ligne);
            grilleIntNew.add(ligneIntNew);
            for(int j=0; j<tailleB; j++){
                ImageView px = new ImageView(this);
                ligne.add(px);

                ligneIntNew.add(0);
                px.setImageResource(R.drawable.rouge);
                int finalI = i;
                int finalJ = j;
                LinearLayout pxL = new LinearLayout(this);
                pxL.addView(px);
                lignL.addView(pxL);

            }
            matL.addView(lignL);
        }

        for(int i=8; i<11; i++){
            for(int j=0; j<12; j++){
                grilleIntNew.get(j).set(i,2);
            }
        }
        for(int j=0; j<9; j++){
            grilleIntNew.get(j).set(11,2);
        }


        for(int i=14; i<18; i++){
            for(int j=0; j<12; j++){
                grilleIntNew.get(j).set(i,2);
            }
        }
        for(int j=0; j<6; j++){
            grilleIntNew.get(j).set(18,2);
        }

        for(int j=2; j<6; j++){
            grilleIntNew.get(j).set(9,1);
        }



        for(int j=11;j<12; j++){
            grilleIntNew.get(j).set(10,1);
        }
        for(int j=0; j<3; j++){
            grilleIntNew.get(j).set(11,1);
        }


        for(int j=3; j<9; j++){
            grilleIntNew.get(j).set(17,1);
        }



        ecran.addView(matL);
        updateImage();

    }

    public void updateImage(){
        for(int coll=0;coll<taille; coll++){
            for(int lign=0; lign<tailleB; lign++){
                Integer color = grilleIntNew.get(coll).get(lign);
                if(color==0){
                    grilleIV.get(coll).get(lign).setImageResource(R.drawable.rouge);
                }
                else if(color==1){
                    grilleIV.get(coll).get(lign).setImageResource(R.drawable.orange);
                }
                else if(color==2){
                    grilleIV.get(coll).get(lign).setImageResource(R.drawable.vert);
                }
            }
        }
    }

}